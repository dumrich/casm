# `casm` Syntax
`casm` uses similar syntax to x64 as that is the only supported platform.

# Basic Syntax
Not all instructions supported on all architectures
```asm
// Single line comment
use std::fs;

// All code outside label considered .data
numbers = [1, 2, 3, 4, 5, 6]
c = 'A'

main:
    movq $12, [rdx] 
    addq $12, [rdx]

    call fs::read("textfile.txt")

    call subNum([rdx])

subNum(quad x):
    subq x, [rdx]
    
```

## Supported instructions

