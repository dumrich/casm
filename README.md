# casm

## What is it?
`casm` is an attempt to give low level assembly higher level features. This includes, but is not limited to:

- Data types
    - UTF-8 Strings
    - Vectors
    - Hash Maps
    - Linked Lists
    - Arrays
    - Trees
    - Heaps
    - Graphs
    - Custom structs
- Standard Library
    - No libc
    - Threading
    - Filesystem
    - env
- Errors
- Portability across operating systems
    - Only the good operating systems (OpenBSD and GNU/Hurd)
- Build system and package manager
    - Like a worse version of Cargo
    - Called `mask` 

## Documentation
Coming soon in Wiki

Check examples for now
