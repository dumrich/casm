use crate::error::Error;
use std::env::{args, var};
use std::path::PathBuf;

use crate::platforms::{Architectures, Systems};

#[derive(Copy, Clone, Debug)]
pub enum Olevel {
    Min,
    Mid,
    Max,
}

#[derive(Clone, Debug)]
pub struct CasmFlags {
    // filename
    file: Option<PathBuf>,

    // Optimize binary
    optimize: Olevel,

    // Output
    output: Option<PathBuf>,

    // Platform data
    os: Systems,
    arch: Architectures,
}

impl CasmFlags {
    fn new(file: Option<PathBuf>, optimize: Olevel, output: Option<PathBuf>) -> CasmFlags {
        CasmFlags {
            file,
            optimize,
            output,
        }
    }

    fn set_optimize(&mut self, optimize: Olevel) {
        self.optimize = optimize;
    }

    fn set_output(&mut self, output: PathBuf) {
        self.output = Some(output);
    }

    fn set_file(&mut self, file: PathBuf) {
        self.file = Some(file);
    }
}

fn display_help() {
    println!("casm 0.0.1\nAbhinav Chavali <abhinavchavali12@gmail.com>\n\ncasm gives standard assembly higher level features including: data types, optimization, and portability\n\nUSAGE:\n    casm [OPTIONS] [asmfile...]\n\nOPTIONS:\n    -O[1-3]\t\t\tHow much to optimize\n    --output <outfile>\t\tPlace the output into <outfile>\n    -h, --help\t\t\tDisplay this message")
}

pub fn parse_args() -> Result<CasmFlags, Error> {
    // quick and dirty argument parser without deps
    let mut flags = CasmFlags::new(None, Olevel::Min, None);

    let args: Vec<String> = args().skip(1).collect();
    let mut skip = false;

    for index in 0..args.len() {
        if !skip {
            let x = &args[index];

            let char1 = &x[0..1];
            let char2 = &x[1..2];

            match char1 {
                "-" => match char2 {
                    "-" => {
                        let arg = &x[2..];
                        match arg {
                            "help" => {
                                display_help();
                                ::std::process::exit(0);
                            }
                            "output" => {
                                let output_flag = args.get(index + 1);
                                skip = true;
                                match output_flag {
                                    Some(f) => flags.set_output(PathBuf::from(f)),
                                    None => {
                                        eprintln!("Could not resolve output file.");
                                        // FIXME: Return Result
                                        ::std::process::exit(1);
                                    }
                                }
                            }
                            other => {
                                return Err(Error::InvalidArgument {
                                    argument: other.to_string(),
                                })
                            }
                        }
                    }
                    _ => {
                        let arg = &x[1..];
                        match arg {
                            "h" => {
                                display_help();
                                ::std::process::exit(0);
                            }
                            "O1" => {
                                flags.set_optimize(Olevel::Min);
                            }
                            "O2" => {
                                flags.set_optimize(Olevel::Mid);
                            }
                            "O3" => {
                                flags.set_optimize(Olevel::Max);
                            }
                            other => {
                                return Err(Error::InvalidArgument {
                                    argument: other.to_string(),
                                })
                            }
                        }
                    }
                },
                _ => {
                    let file = PathBuf::from(&x[..]);
                    if file.exists() {
                        flags.set_file(file);
                    } else {
                        return Err(Error::FileNotFound { file });
                    }
                }
            }
        } else {
            skip = false;
        }
    }
    if flags.file.is_none() {
        eprintln!("Could not resolve asmfile.");
        ::std::process::exit(1);
    }
    if flags.output.is_none() {
        if let Some(mut m) = flags.file.clone() {
            m.set_extension("");
            flags.set_output(m);
        }
    }
    Ok(flags)
}

pub fn run() -> Result<(), Error> {
    let args = parse_args()?;
    Ok(())
}
