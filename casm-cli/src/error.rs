use std::path::PathBuf;

use snafu::{Backtrace, ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu_display("Unrecognized argument: {}", "argument")]
    InvalidArgument { argument: String },

    #[snafu_display("File not found: {}", "file.display()")]
    FileNotFound { file: PathBuf },
}
