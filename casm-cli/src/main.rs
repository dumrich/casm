use error::Error;

mod cli;
mod error;
mod platforms;

fn main() {
    match cli::run() {
        Ok(_) => (),
        Err(e) => {
            handle_error(e);
        }
    };
}

fn handle_error(e: Error) {
    eprintln!("{}", e);
}
