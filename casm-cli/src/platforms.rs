#[derive(Copy, Clone, Debug)]
pub enum Systems {
    OpenBSD,
    FreeBSD,
    Linux,
    MacOS,
}

#[derive(Copy, Clone, Debug)]
pub enum Architectures {
    X86,
    X64,
    ARM,
    RiscV,
}
