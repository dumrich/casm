typedef struct LinkedList {
    void* data;
    struct LinkedList* prev;
    struct LinkedList* next;
} LinkedList;

void append(LinkedList* s, void)
